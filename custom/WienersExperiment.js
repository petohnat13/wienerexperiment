var canvas;
var lightColor;
var waveLengthGlobal;
var engine;
var context;
var time = {
    actual: 0,
    total: 0,
    speed: 1
};

//Init canvas and values
function canvasInit() {
    canvas = new fabric.Canvas('canvas');
    canvas.preserveObjectStacking = true;
    canvas.setHeight(675);
    canvas.setWidth(820);
    context = canvas.getContext("2d");
    lightColor = getWavelengthColor(600);
    waveLengthGlobal = 600;
    engine = new Engine();
}

//Sets canvas width and height
function setCanvasDimensions() {
    if (window.innerWidth < 820) canvas.setWidth(820);
    else canvas.setWidth(window.innerWidth);
    if (window.innerHeight < 675) canvas.setHeight(675);
    else canvas.setHeight(window.innerHeight);
}

//Starts app when window loads
window.onload = function () {
    canvasInit();
    createControlPanelGUI();
    repaint();
};

//Options menu variables with init values
var Options = function () {
    this.optWavelength = waveLengthGlobal;
    this.optAngle = 15;
    this.optTimeSpeed = 1;
    this.exampleFlag = false;
};

//Add options menu datGUI
function createControlPanelGUI() {
    var opts = new Options();
    var gui = new dat.GUI();

    var optWavelength = gui.add(opts, 'optWavelength', 400, 700).name('Wavelength [nm]');
    var optAngle = gui.add(opts, 'optAngle', 0, 90).name("Film angle [&deg]");
    var optTimeSpeed = gui.add(opts, 'optTimeSpeed', 0, 25).name("Animation speed");

    //If wavelength changes, save new values and repaint the canvas
    optWavelength.onChange(function (value) {
        waveLengthGlobal = value;
        lightColor = getWavelengthColor(value);
    });

    optTimeSpeed.onChange(function (value) {
        time.speed = value;
    });

    optAngle.onChange(function (value) {
        engine.filmAngle = value;
    })
}

//Repaints the canvas
function repaint() {
    canvas.clear();
    canvas.backgroundColor = "#1a1a1a";
    setCanvasDimensions();
    drawLegend();
    drawLightSourceRectangle();
    drawHelpLines();
    drawMirrorRectangle();

    engine.update();
    engine.render();
    requestAnimationFrame(repaint);
}

//Counts how many lines and where should be drawn.
//Calls to draw help lines at yλ/4 and dotted help lines at yλ/2
function drawHelpLines() {
    var numberOfLines = Math.floor(3000 / (waveLengthGlobal / 4));
    if (numberOfLines % 2 == 0)
        numberOfLines -= 1;

    var index;
    var posY;
    for (index = 1; index <= numberOfLines; index += 2) {
        posY = 50 + (index * (waveLengthGlobal / 4)) / 5;
        drawLine([0, posY, 600, posY], numberOfLines - index + 1);
        if (index <= numberOfLines) {
            if ((posY + (waveLengthGlobal / 4) / 5) < 650)
                drawDottedLine([0, posY + (waveLengthGlobal / 4) / 5, 600, posY + (waveLengthGlobal / 4) / 5]);
        }
    }
}

//Draws line with description
function drawLine(coord, index) {
    "use strict";
    var line = new fabric.Line(coord, {
        fill: "white",
        stroke: "white",
        strokeWidth: 2,
        opacity: 0.5,
        selectable: false,
        objectCaching: false
    });
    canvas.centerObjectH(line);
    canvas.add(line);
    canvas.bringForward(line);

    var description = new fabric.Text(index + "λ/4", {
        fontSize: 14,
        left: canvas.getCenter().left + 310,
        top: coord[1] - 7,
        fontFamily: "Montserrat",
        fill: "white",
        selectable: false,
        objectCaching: false
    });
    canvas.add(description);
    canvas.bringForward(description);
}

//Draws dotted line
function drawDottedLine(coord) {
    "use strict";
    var line = new fabric.Line(coord, {
        fill: "white",
        stroke: "white",
        strokeWidth: 2,
        opacity: 0.5,
        selectable: false,
        strokeDashArray: [5, 5],
        objectCaching: false
    });
    canvas.centerObjectH(line);
    canvas.add(line);
    canvas.bringForward(line);
}

//Draws rectangle that represents light source
function drawLightSourceRectangle() {
    "use strict";
    var box = new fabric.Rect({
        width: 600,
        height: 25,
        fill: lightColor,
        originX: "center",
        originY: "center"
    });

    var title = new fabric.Text("Light Source", {
        fontSize: 15,
        originX: "center",
        originY: "center",
        fill: "black",
        fontFamily: "Montserrat"
    });

    var titleBox = new fabric.Group([box, title], {
        selectable : false,
        top: 25,
        left: 0,
        objectCaching: false
    });

    canvas.centerObjectH(titleBox);
    canvas.add(titleBox);
    canvas.bringForward(titleBox);
}

//Draws rectangle that represents mirror
function drawMirrorRectangle() {
    "use strict";
    var box = new fabric.Rect({
        width: 600,
        height: 25,
        fill: "#489eba",
        originX: "center",
        originY: "center"
    });

    var title = new fabric.Text("Mirror", {
        fontSize: 15,
        fill: "black",
        fontFamily: "Montserrat",
        originX: "center",
        originY: "center"
    });

    var titleBox = new fabric.Group([box, title], {
        selectable : false,
        top: 650,
        left: 0,
        objectCaching: false
    });

    canvas.centerObjectH(titleBox);
    canvas.add(titleBox);
    canvas.bringForward(titleBox);
}

//Draws the legend with description
function drawLegend(){
    "use strict";
    var boxO = new fabric.Rect({
        width: 12,
        height: 12,
        fill: "rgb(255,105,97)",
        originX: "center",
        originY: "center",
        left: canvas.getCenter().left - 410
    });

    var titleO = new fabric.Text("Original wave", {
        fontSize: 12,
        originX: "center",
        originY: "center",
        fill: "white",
        fontFamily: "Montserrat",
        left: canvas.getCenter().left - 360
    });

    var boxR = new fabric.Rect({
        width: 12,
        height: 12,
        fill: "rgb(97,168,255)",
        originX: "center",
        originY: "center",
        top: 20,
        left: canvas.getCenter().left - 410
    });

    var titleR = new fabric.Text("Reflected wave", {
        fontSize: 12,
        originX: "center",
        originY: "center",
        fill: "white",
        fontFamily: "Montserrat",
        top: 20,
        left: canvas.getCenter().left - 355
    });

    var boxRe = new fabric.Rect({
        width: 12,
        height: 12,
        fill: lightColor,
        originX: "center",
        originY: "center",
        top: 40,
        left: canvas.getCenter().left - 410
    });

    var titleRe = new fabric.Text("Resulting wave", {
        fontSize: 12,
        originX: "center",
        originY: "center",
        fill: "white",
        fontFamily: "Montserrat",
        top: 40,
        left: canvas.getCenter().left - 356
    });

    var legend = new fabric.Group([boxO, titleO, boxR, titleR, boxRe, titleRe], {
        top: 325,
        selectable: false,
        objectCaching: false
    });

    canvas.add(legend);
    canvas.bringForward(legend);
}

//Returns the color of wavelength [Source: http://scienceprimer.com/javascript-code-convert-light-wavelength-color]
function getWavelengthColor(wl) {
    "use strict";
    var R, G, B, alpha;

    if (wl >= 380 && wl < 440) {
        R = -1 * (wl - 440) / (440 - 380);
        G = 0;
        B = 1;
    } else if (wl >= 440 && wl < 490) {
        R = 0;
        G = (wl - 440) / (490 - 440);
        B = 1;
    } else if (wl >= 490 && wl < 510) {
        R = 0;
        G = 1;
        B = -1 * (wl - 510) / (510 - 490);
    } else if (wl >= 510 && wl < 580) {
        R = (wl - 510) / (580 - 510);
        G = 1;
        B = 0;
    } else if (wl >= 580 && wl < 645) {
        R = 1;
        G = -1 * (wl - 645) / (645 - 580);
        B = 0.0;
    } else if (wl >= 645 && wl <= 780) {
        R = 1;
        G = 0;
        B = 0;
    } else {
        R = 0;
        G = 0;
        B = 0;
    }

    //Intensity is lower at the edges of the visible spectrum.
    if (wl > 780 || wl < 380) {
        alpha = 0;
    } else if (wl > 700) {
        alpha = (780 - wl) / (780 - 700);
    } else if (wl < 420) {
        alpha = (wl - 380) / (420 - 380);
    } else {
        alpha = 1;
    }

    return "rgba(" + (R * 100) + "%," + (G * 100) + "%," + (B * 100) + "%, " + alpha + ")";
}

//Object representing wave
var Wave = function (waveLength, phaseVelocity, waveType) {
    "use strict";
    this.waveLength = waveLength/5;
    this.phaseVelocity = phaseVelocity;
    this.waveType = waveType;
    this.startY = 25 + 26;
};
Wave.prototype = {
    getEquationValue: function (z, t) {
        "use strict";
        return this.amplitude * Math.sin(this.omega * t - this.waveNuber * z);
    },
    setProportions: function (startX, height) {
        "use strict";
        this.startX = startX;
        this.height = height;
    },
    update: function () {
        this.amplitude = this.waveLength/2;
        this.frequency = this.phaseVelocity/this.waveLength;
        this.waveNuber = (2 * Math.PI)/this.waveLength;
        this.omega = (2 * Math.PI) * this.frequency;
    }
};

//Object representing standing wave
var StandingWave = function (originalWave, reflectedWave, waveType) {
    this.originalWave = originalWave;
    this.reflectedWave = reflectedWave;
    this.startY = 25 + 26;
    this.waveType = waveType;
};
StandingWave.prototype = {
    getEquationValue: function (z, t) {
        "use strict";
        return this.originalWave.getEquationValue(z, t) + this.reflectedWave.getEquationValue(z, t);
    },
    getAmplitude: function () {
        "use strict";
        return this.originalWave.amplitude + this.reflectedWave.amplitude;
    },
    setProportions: function (startX, height) {
        "use strict";
        this.startX = startX;
        this.height = height;
    }
};

//Function that renders given wave
function renderWave(wave){
    "use strict";
    var wavePos = 0;

    context.save();
    context.translate(wave.startX, wave.startY);
    context.lineWidth = 2;
    context.beginPath();

    for (var zCoord = 0; zCoord <= wave.height; zCoord += 1){
        wavePos = wave.getEquationValue(zCoord*5, time.total)/5;
        if (zCoord == 0){
            context.moveTo(wavePos, zCoord);
        }
        context.lineTo(wavePos, zCoord);
    }

    switch (wave.waveType) {
        case "original":
            context.strokeStyle = "rgb(255,105,97)";
            break;
        case "reflected":
            context.strokeStyle = "rgb(97,168,255)";
            break;
        case "standing":
            context.strokeStyle = lightColor;
            break;
    }

    context.stroke();
    context.restore();
}

function renderFilm(){
    var film = new fabric.Rect({
        width: 600,
        height: 600,
        top: 50,
        left: canvas.getCenter().left - 300,
        fill: "black",
        selectable: false,
        objectCaching: false
    });

    var gradientArray = [];
    var colorGrad = 0, pos = 0, grad;

    for (var zCoord = 0; zCoord < 600; zCoord += 1) {
        waveValue = engine.standingWave.getEquationValue(zCoord*5, time.total);
        waveValue = Math.abs(waveValue);

        colorGrad = 300 - Math.round((waveValue/engine.standingWave.getAmplitude())*255);
        pos = zCoord/600;
        grad = {offset: pos, color: "rgba(" + colorGrad + ", " + colorGrad + ", " + colorGrad + ", 1)"};
        gradientArray.push(grad);
    }

    film.setGradient("fill", {
        x1:0, y1:0, x2:0, y2:600,
        selectable: false,
        objectCashing: false
    });
    film.fill.colorStops = gradientArray;

    film.clipTo = function (context) {
        context.save();
        context.setTransform(1,0,0,1,0,0);
        context.translate(canvas.getCenter().left - 300, 650);
        context.rotate(-(Math.PI / 180) * engine.filmAngle);
        context.rect(0, -8, 3000, 8);
        context.restore();
    };

    canvas.add(film);
};

//Main engine managing the waves and film
var Engine = function() {
    "use strict";
    this.startTime = 0;
    this.originalWave = new Wave(waveLengthGlobal, 20, "original");
    this.reflectedWave = new Wave(waveLengthGlobal, -20, "reflected");
    this.standingWave = new StandingWave(this.originalWave, this.reflectedWave, "standing");

    this.filmAngle = 15;
};
Engine.prototype = {
    update: function () {
        "use strict";
        var currentTime = new Date().getTime();
        time.total += Math.abs((currentTime - time.actual) / 1000 * time.speed);
        time.actual = currentTime;

        this.originalWave.setProportions(canvas.getCenter().left - 150, 600);
        this.originalWave.waveLength = waveLengthGlobal;
        this.originalWave.update();

        this.reflectedWave.setProportions(canvas.getCenter().left - 150, 600);
        this.reflectedWave.waveLength = waveLengthGlobal;
        this.reflectedWave.update();

        this.standingWave.setProportions(canvas.getCenter().left + 150, 600);
    },
    render: function () {
        "use strict";
        context.save();
        renderFilm();
        renderWave(this.reflectedWave);
        renderWave(this.originalWave);
        renderWave(this.standingWave);
        context.restore();
    }
};